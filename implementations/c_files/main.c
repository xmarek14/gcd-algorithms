#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "ctype.h"
#include "gmp.h"

#include "helper_funcs.h"

#include "long_algorithms/basic_algorithms.h"
#include "long_algorithms/euclid_lower_bits.h"
#include "long_algorithms/float_shift.h"
#include "long_algorithms/lehmer.h"
#include "long_algorithms/lehmer_binary.h"
#include "long_algorithms/lehmer_lookup.h"
#include "long_algorithms/lehmer-lower_bits.h"

#include "gmp_algorithms/basic_algorithms_gmp.h"
#include "gmp_algorithms/float_shift_gmp.h"
#include "gmp_algorithms/lehmer_binary_gmp.h"
#include "gmp_algorithms/lehmer_gmp.h"
#include "gmp_algorithms/lehmer_lower_bits_gmp.h"

void runAlgorithms(int tests) {

    int shift = 8;
    int _shift = 50;
    int bits = shift;
    int sizeOfTable = (int) pow(pow(2, shift), 2) * 4;
    int* table = malloc(sizeof(int) * sizeOfTable);
    precomputation(table, bits);
    printf("precomputation done\n");

    int k = 4;
    int* combinedTable = malloc((1 << k) * (1 << k));
    combinedPrecomputation(k, combinedTable);

    srand(time(0));
    long *inputsU, *inputsV;
    inputsU = (long*) malloc(tests * sizeof (long));
    inputsV = (long*) malloc(tests * sizeof (long));
    for (int i = 0; i < tests; ++i) {
        inputsU[i] = rand() % 0x7FFFFFFF;
        inputsU[i] = (inputsU[i] << 28) + rand();
        inputsV[i] = rand() % 0x7FFFFFFF;
        inputsV[i] = (inputsV[i] << 28) + rand();
    }

    time_t start, end;
    int sum = 0;

    printf("Running tests on long numbers:\n"
           "args: tests = %d\n\n", tests);

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) euclid(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Euclid's algorithm:     %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) leastReminder(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Negative modulo:        %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) division(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Division algorithm:     %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) shifting(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Shifting division:      %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) binary(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Normal binary:          %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) binaryLookup(inputsU[i], inputsV[i]);
    }
    end = clock();
    printf("%d Lookup binary:          %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) combinedEuclid(inputsU[i], inputsV[i], k, combinedTable);
    }
    end = clock();
    printf("%d Euclid's on lower bits: %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) lehmerOrig(inputsU[i], inputsV[i], _shift);
    }
    end = clock();
    printf("%d Original Lehmer's:      %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) myLehmer(inputsU[i], inputsV[i], _shift);
    }
    end = clock();
    printf("%d Modified Lehmer's:      %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) binaryLehmer(inputsU[i], inputsV[i], _shift);
    }
    end = clock();
    printf("%d Binary Lehmer's:        %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) binaryLehmerWithZeros(inputsU[i], inputsV[i], _shift, 4);
    }
    end = clock();
    printf("%d Binary L. with zeros:   %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) lookupLehmer(inputsU[i], inputsV[i], shift, table);
    }
    end = clock();
    printf("%d Lookup Lehmer's:        %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) binaryLookupLehmer(inputsU[i], inputsV[i], shift, table);
    }
    end = clock();
    printf("%d Binary lookup Lehmer's: %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;

    start = clock();
    for (int i = 0; i < tests; ++i) {
        sum += (int) lowerBitsLehmer(inputsU[i], inputsV[i], _shift);
    }
    end = clock();
    printf("%d Lehmer's on lower bits: %f\n", sum, (double) (end - start) / CLOCKS_PER_SEC);
    sum = 0;


    /* COMPARING FOR DEBUG PURPOSES
    {
        for (int i = 0; i < tests; ++i) {
            int x = (int) division(inputsU[i], inputsV[i]);
            int y = (int) shifting(inputsU[i], inputsV[i]);
            if (x != y) {
                printf("inputs: %ld %ld\n", inputsU[i], inputsV[i]);
                printf("right: %d, wrong %d\n", y, x);
            }
        }
    }
     */

}

void runGmpAlgorithms(int tests, int inputSize, int shift, int zeros) {

    mpz_t u, v, g1;
    mpz_init(u);
    mpz_init(v);
    mpz_init(g1);

    gmp_randstate_t state;
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, 10000U);

    int g[10] =  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    double times[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    time_t start, end;

    printf("Tests of GMP algorithms on %d bit numbers:\n"
           "args: tests = %d, shift = %d, zeros = %d\n\n",
           inputSize, tests, shift, zeros);

    for (int i = 0; i < tests; ++i) {
        mpz_urandomb(u, state, inputSize);
        mpz_add_ui(u, u, 1);
        mpz_urandomb(v, state, inputSize);
        mpz_add_ui(v, v, 1);

        start = clock();
        euclidGmp(g1, u, v);
        end = clock();
        g[0] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[0] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        leastReminderGmp(g1, u, v);
        end = clock();
        g[1] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[1] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        divisionGmp(g1, u, v);
        end = clock();
        g[2] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[2] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        shiftingGmp(g1, u, v);
        end = clock();
        g[3] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[3] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        binaryGmp(g1, u, v);
        end = clock();
        g[4] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[4] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        myLehmerGmp(g1, u, v, shift);
        end = clock();
        g[5] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[5] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        binaryLehmerGmp(g1, u, v, shift);
        end = clock();
        g[6] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[6] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        binaryLehmerWithZerosGmp(g1, u, v, shift, zeros);
        end = clock();
        g[7] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[7] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        binaryLehmerNoLimitsGmp(g1, u, v, shift, zeros);
        end = clock();
        g[8] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[8] += (double) (end - start) / CLOCKS_PER_SEC;

        start = clock();
        lowerBitsLehmerGmp(g1, u, v, shift);
        end = clock();
        g[9] += mpz_get_ui(g1);
        mpz_set_ui(g1, 0);
        times[9] += (double) (end - start) / CLOCKS_PER_SEC;

    }

    printf("%d\tEuclid's:             %f s\n"
           "%d\tLeast reminder:       %f s\n"
           "%d\tDivision:             %f s\n"
           "%d\tShifting:             %f s\n"
           "%d\tBinary:               %f s\n"
           "%d\tLehmer's algorithm:   %f s\n"
           "%d\tBinary Lehmer's:      %f s\n"
           "%d\tBinary L. with zeros: %f s\n"
           "%d\tUnlimited Lehmer's:   %f s\n"
           "%d\tLower bits Lehmer's:  %f s\n",
           g[0], times[0], g[1], times[1], g[2], times[2], g[3], times[3],
           g[4], times[4], g[5], times[5], g[6], times[6], g[7], times[7],
           g[8], times[8], g[9], times[9]);

    mpz_clear(u);
    mpz_clear(v);
    mpz_clear(g1);
    gmp_randclear(state);
}

void computeLongGcd() {
    long u, v;
    printf("Enter u: ");
    if (scanf("%ld", &u) <= 0) {
        printf("You did not enter a number!\n");
        return;
    }
    printf("Enter v: ");
    if (scanf("%ld", &v) <= 0) {
        printf("You did not enter a number!\n");
        return;
    }
    printf("Gcd of %ld, %ld is %ld.\n", u, v, binaryLookup(u, v));
}

void computeGmpGcd() {
    mpz_t g, u, v;
    mpz_init(g);
    mpz_init(u);
    mpz_init(v);
    printf("Enter u: ");
    gmp_scanf("%Zd", u);
    printf("Enter v: ");
    gmp_scanf("%Zd", v);
    binaryLehmerWithZerosGmp(g, u, v, 63, 63);
    gmp_printf("Gcd of %Zd, %Zd is %Zd.\n", u, v, g);

    mpz_clear(g);
    mpz_clear(u);
    mpz_clear(v);
}

int main(int argc, char *argv[]) {

    if (argc != 2 && argc != 5) {
        fprintf(stderr, "invalid arguments\n"
                        "for comparison of long inputs:      \"./main [tests]\"\n"
                        "for comparison of unlimited inputs: \"./main [tests input_size shift trailing_zeros]\"\n"
                        "for custom long input:              \"./main long\"\n"
                        "for custom unlimited input:         \"./main gmp\"\n");
        exit(1);
    }

    if ( strcmp(argv[1], "long") == 0) {
        computeLongGcd();
        return 0;
    } else if ( strcmp(argv[1], "gmp") == 0) {
        computeGmpGcd();
        return 0;
    } else if (!isNumber(argv[1])) {
        fprintf(stderr, "argument \"tests\" must be a number\n");
        exit(1);
    }

    int tests = (int) strtol(argv[1], NULL, 10);
    int inputSize = 500;
    int shift = 50;
    int zeros = 5;
    if (argc == 5) {
        if (isNumber(argv[2])) {
            printf("Parsing input size...\n");
            inputSize = (int) strtol(argv[2], NULL, 10);
        }
        if (isNumber(argv[3])) {
            printf("Parsing shift...\n");
            shift = (int) strtol(argv[3], NULL, 10);
        }
        if (isNumber(argv[4])) {
            printf("Parsing trailing zeros...\n");
            zeros = (int) strtol(argv[4], NULL, 10);
        }
        runGmpAlgorithms(tests, inputSize, shift, zeros);
    } else {
        runAlgorithms(tests);
    }

    return 0;
}
