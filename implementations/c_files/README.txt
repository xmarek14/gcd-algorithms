compilation command:
gcc -O2 -o main main.c long_algorithms/basic_algorithms.c helper_funcs.c long_algorithms/float_shift.c long_algorithms/lehmer.c long_algorithms/lehmer-lower_bits.c long_algorithms/lehmer_lookup.c long_algorithms/lehmer_binary.c gmp_algorithms/basic_algorithms_gmp.c gmp_algorithms/float_shift_gmp.c gmp_algorithms/lehmer_gmp.c gmp_algorithms/lehmer_binary_gmp.c gmp_algorithms/lehmer_lower_bits_gmp.c long_algorithms/euclid_lower_bits.c -lm -lgmp



run command for algorithms with long numbers:
./main [number_of_iterations]

run command for algorithms implemented in GMP library:
./main [number_of_iterations input_size shift trailing_zeros]

run command for computation gcd of two custom long numbers:
./main long

run command for computation gcd of two custom numbers of unlimited size:
./main gmp