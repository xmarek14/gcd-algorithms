#ifndef GCD_ALGORITHMS_LEHMER_GMP_H
#define GCD_ALGORITHMS_LEHMER_GMP_H

/**
 * Modified version of Lehmer's algorithm implemented in GMP library
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 * @param shift number of the most significant bits to work with
 */
void myLehmerGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift);

#endif //GCD_ALGORITHMS_LEHMER_GMP_H
