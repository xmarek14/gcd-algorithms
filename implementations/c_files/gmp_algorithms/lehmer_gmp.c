#include "stdbool.h"
#include "gmp.h"

#include "lehmer_gmp.h"
#include "../helper_funcs.h"

#define MASK 63

void myLehmerGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift) {

    mpz_t tmp, tmp1, _u, _v;

    mpz_init(tmp);
    mpz_init(tmp1);
    mpz_init(_u);
    mpz_init(_v);

    mpz_set(_u, u);
    mpz_set(_v, v);

    if (mpz_cmp(_u, _v) < 0) {
        mpz_swap(_u, _v);
    }

    unsigned long uLen = mpz_sizeinbase(_u, 2);
    unsigned long vLen = mpz_sizeinbase(_v, 2);

    unsigned long k;
    mpz_t u1, v1, u2, v2, q;
    mpz_init(u1);
    mpz_init(v1);
    mpz_init(u2);
    mpz_init(v2);
    mpz_init(q);

    while (vLen >= shift) {

        k = uLen - shift;
        mpz_div_2exp(u1, _u, k);
        mpz_div_2exp(v1, _v, k);

        mpz_mul_2exp(u2, u1, k);
        mpz_sub(u2, _u, u2);
        mpz_mul_2exp(v2, v1, k);
        mpz_sub(v2, _v, v2);

        mpz_t a, b, c, d;
        mpz_init_set_ui(a, 1);
        mpz_init_set_ui(b, 0);
        mpz_init_set_ui(c, 0);
        mpz_init_set_ui(d, 1);

        while (true) {

            if (mpz_cmp(v1, d) < 0 || mpz_cmp(v1, c) < 0) {
                break;
            }

            mpz_fdiv_q(q, u1, v1);

            mpz_set(tmp, a);
            mpz_set(a, c);
            mpz_mul(tmp1, q, c);
            mpz_sub(c, tmp, tmp1);

            mpz_set(tmp, b);
            mpz_set(b, d);
            mpz_mul(tmp1, q, d);
            mpz_sub(d, tmp, tmp1);

            mpz_set(tmp, u1);
            mpz_set(u1, v1);
            mpz_mul(tmp1, q, v1);
            mpz_sub(v1, tmp, tmp1);
        }

        if (mpz_cmp_ui(b, 0) == 0) {
            mpz_set(tmp, _v);
            mpz_mod(_v, _u, _v);
            mpz_set(_u, tmp);
        } else {
            mpz_mul_2exp(u1, u1, k);
            mpz_mul(tmp, u2, a);
            mpz_mul(tmp1, v2, b);
            mpz_add(tmp, u1, tmp);
            mpz_add(_u, tmp, tmp1);
            mpz_abs(_u, _u);

            mpz_mul_2exp(v1, v1, k);
            mpz_mul(tmp, u2, c);
            mpz_mul(tmp1, v2, d);
            mpz_add(tmp, v1, tmp);
            mpz_add(_v, tmp, tmp1);
            mpz_abs(_v, _v);
        }

        if (mpz_cmp(_u, _v) < 0) {
            mpz_swap(_u, _v);
        }
        uLen = mpz_sizeinbase(_u, 2);
        vLen = mpz_sizeinbase(_v, 2);

    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_set(tmp, _v);
        mpz_mod(_v, _u, _v);
        mpz_set(_u, tmp);
    }

    mpz_set(g, _u);
}
