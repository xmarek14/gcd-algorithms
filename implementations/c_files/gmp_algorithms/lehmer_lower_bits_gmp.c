#include "gmp.h"

#include "lehmer_lower_bits_gmp.h"

void lowerBitsLehmerGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift) {

    if (shift > 63) {
        mpz_set_ui(g, 0);
        return;
    }

    mpz_t uZeros, vZeros, _u, _v, tmp1, tmp2, tmp3;

    mpz_init(uZeros);
    mpz_init(vZeros);
    mpz_init(_u);
    mpz_init(_v);
    mpz_init(tmp1);
    mpz_init(tmp2);
    mpz_init(tmp3);

    mpz_set(_u, u);
    mpz_set(_v, v);

    while (mpz_even_p(_u)) {
        mpz_add_ui(uZeros, uZeros, 1);
        mpz_div_2exp(_u, _u, 1);
    }
    while (mpz_even_p(_v)) {
        mpz_add_ui(vZeros, vZeros, 1);
        mpz_div_2exp(_v, _v, 1);
    }

    if (mpz_cmp(_u, _v) < 0) {
        mpz_swap(_u, _v);
    }

    unsigned long vLen = mpz_sizeinbase(_v, 2);

    long u3, v3, tmp;
    while (vLen >= shift){

        u3 = mpz_get_ui(_u);
        v3 = mpz_get_ui(_v);

        long a = 1, b = 0, c = 0, d = 1;
        /**
         * In purpose of simplification of the masking, index does not hold
         * the index of bit, but value 2^real_index.
         */
        int index = 1;
        while (v3 > 0) {

            if (u3 & index & v3) {
                tmp = a;
                a = c;
                c = tmp - c;

                tmp = b;
                b = d;
                d = tmp - d;

                tmp = u3;
                u3 = v3;
                v3 = tmp - v3;
            } else if (u3 & index) {
                u3 <<= 1;
                a <<= 1;
                b <<= 1;
                index <<= 1;
            } else if (v3 & index) {
                v3 <<= 1;
                c <<= 1;
                d <<= 1;
                index <<= 1;
            } else {
                index <<= 1;
                a <<= 1;
                b <<= 1;
                c <<= 1;
                d <<= 1;
            }
        }

        if (b == 0) {
            mpz_set(tmp1, _v);
            mpz_mod(_v, _u, _v);
            mpz_set(_u, tmp1);
        } else {
            mpz_mul_si(tmp1, _u, a);
            mpz_mul_si(tmp2, _v, b);
            mpz_add(tmp3, tmp1, tmp2);
            mpz_abs(tmp3, tmp3);

            mpz_mul_si(tmp1, _u, c);
            mpz_mul_si(tmp2, _v, d);
            mpz_add(tmp1, tmp1, tmp2);
            mpz_abs(tmp1, tmp1);

            mpz_set(_u, tmp3);
            mpz_set(_v, tmp1);
        }

        mpz_div_2exp(_u, _u, mpz_scan1(_u, 0));
        mpz_div_2exp(_v, _v, mpz_scan1(_v, 0));

        if (mpz_cmp(_u, _v) < 0) {
            mpz_swap(_u, _v);
        }

        vLen = mpz_sizeinbase(_v, 2);
    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_set(tmp1, _v);
        mpz_mod(_v, _u, _v);
        mpz_set(_u, tmp1);
    }

    if (mpz_cmp(uZeros, vZeros) > 0) {
        mpz_set(tmp1, _u);
        while (mpz_cmp_ui(vZeros, 0)) {
            mpz_mul_2exp(tmp1, tmp1, 1);
            mpz_sub_ui(vZeros, vZeros, 1);
        }
        mpz_set(g, tmp1);
    } else {
        mpz_set(tmp1, _u);
        while (mpz_cmp_ui(uZeros, 0)){
            mpz_mul_2exp(tmp1, tmp1, 1);
            mpz_sub_ui(uZeros, uZeros, 1);
        }
        mpz_set(g, tmp1);
    }
}