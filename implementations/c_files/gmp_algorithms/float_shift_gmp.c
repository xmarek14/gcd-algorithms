#include "gmp.h"
#include "stdbool.h"

#include "float_shift_gmp.h"
#include "../helper_funcs.h"

#define DECIMAL_PLACES 23
#define VAL(P) mpz_scan1(P, 0)

unsigned int getShiftGmp(mpf_t f) {

    double fDouble = mpf_get_d(f);
    unsigned int fInt = (unsigned int) ((fDouble - (int) fDouble) * (1 << DECIMAL_PLACES));

    if (fInt == 0) {
        return 0;
    }

    int sequence = 1;
    while ((fInt >> 1) & fInt) {
        sequence++;
        fInt &= fInt >> 1;
    }

    while (true) {
        int zeros = trailingZeros(fInt);
        if (!(fInt & (0xFFFFFFFF << (zeros + 1)))) {
            return (DECIMAL_PLACES - (zeros + sequence));
        }
        fInt &= 0xFFFFFFFF << (zeros + 1);
    }
}

void shiftingGmp(mpz_t g, mpz_t u, mpz_t v) {

    mpz_t tmp_z, tmp_z1, tmp_z2, _u, _v;
    mpf_t f, fu, fv, tmp_f, tmp_f1, tmp_f2;
    unsigned int uZeros, vZeros, minZeros, shift;

    mpz_init(tmp_z);
    mpz_init(tmp_z1);
    mpz_init(tmp_z2);
    mpz_init(_u);
    mpz_init(_v);
    mpf_init(f);
    mpf_init(fu);
    mpf_init(fv);
    mpf_init(tmp_f);
    mpf_init(tmp_f1);
    mpf_init(tmp_f2);

    mpz_set(_u, u);
    mpz_set(_v, v);

    //get trailing zeros and shift u and v
    uZeros = VAL(_u);
    vZeros = VAL(_v);
    minZeros = (uZeros < vZeros) * uZeros + (vZeros <= uZeros) * vZeros;
    mpz_div_2exp(_u, _u, uZeros);
    mpz_div_2exp(_v, _v, vZeros);

    mpf_set_z(fu, _u);
    mpf_set_z(fv, _v);
    if (mpf_cmp_ui(fv, 0) == 0) {
        mpz_set_f(g, fu);
        return;
    }
    mpf_div(f, fu, fv);

    while (mpz_sgn(_v)) {
        shift = getShiftGmp(f);
        mpf_mul_2exp(f, f, shift);
        mpz_set_f(tmp_z, f);
        mpz_mul_ui(tmp_z, tmp_z, 2);
        mpf_set_z(tmp_f1, tmp_z);
        mpf_mul_ui(tmp_f2, f, 2);

        mpf_sub(tmp_f, tmp_f2, tmp_f1);
        mpz_set_f(tmp_z2, tmp_f);
        mpz_set_f(tmp_z1, f);

        mpz_add(tmp_z2, tmp_z1, tmp_z2);
        mpz_mul(tmp_z2, tmp_z2, _v);
        mpz_mul_2exp(tmp_z1, _u, shift);

        mpz_sub(tmp_z, tmp_z1, tmp_z2);
        mpz_mul_si(tmp_z, tmp_z, mpz_sgn(tmp_z));
        mpz_set(_u, _v);
        mpz_set(_v, tmp_z);

        if (mpz_sgn(_v) == 0) {
            break;
        }

        mpf_set_z(fu, _u);
        mpf_set_z(fv, _v);
        mpf_div(f, fu, fv);
    }

    uZeros = VAL(_u);
    mpz_div_2exp(_u, _u, uZeros);
    mpz_mul_2exp(g, _u, minZeros);

    mpz_clear(tmp_z);
    mpz_clear(tmp_z1);
    mpz_clear(tmp_z2);
    mpz_clear(_u);
    mpz_clear(_v);
    mpf_clear(f);
    mpf_clear(fu);
    mpf_clear(fv);
    mpf_clear(tmp_f);
    mpf_clear(tmp_f1);
    mpf_clear(tmp_f2);
}

void divisionGmp(mpz_t g, mpz_t u, mpz_t v) {
    mpf_t f, fu, fv;
    mpz_t tmp1;
    mpz_t _f, _u, _v;

    mpf_init(f);
    mpf_init(fu);
    mpf_init(fv);
    mpz_init(_f);
    mpz_init(_u);
    mpz_init(_v);
    mpz_init(tmp1);

    mpz_set(_u, u);
    mpz_set(_v, v);
    mpf_set_z(fu, _u);
    mpf_set_z(fv, _v);
    mpf_div(f, fu, fv);
    mpz_set_f(_f, f);

    while (mpf_cmp_z(f, _f) != 0) {

        mpz_mul(tmp1, _f, _v);
        mpz_sub(tmp1, _u, tmp1);
        mpz_set(_u, _v);
        mpz_set(_v, tmp1);

        mpf_set_z(fu, _u);
        mpf_set_z(fv, _v);
        mpf_div(f, fu, fv);
        mpz_set_f(_f, f);
    }

    mpf_clear(f);
    mpf_clear(fu);
    mpf_clear(fv);
    mpz_clear(_f);
    mpz_clear(tmp1);

    mpz_set(g, _v);
}
