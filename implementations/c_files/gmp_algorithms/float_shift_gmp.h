#ifndef GCD_ALGORITHMS_FLOAT_SHIFT_GMP_H
#define GCD_ALGORITHMS_FLOAT_SHIFT_GMP_H

/**
 * Helper function for shifting_gmp, searches for optimal zero sequence
 * @param f decimal number to search for longest zero series in
 * @return optimal shift to get the most zeros behind decimal point
 */
unsigned int getShiftGmp(mpf_t f);

/**
 * Computes ratio u/v and uses shifted value to create u', v'
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 */
void shiftingGmp(mpz_t g, mpz_t u, mpz_t v);

/**
 * Different implementation of Euclid's algorithm
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 */
void divisionGmp(mpz_t g, mpz_t u, mpz_t v);

#endif //GCD_ALGORITHMS_FLOAT_SHIFT_GMP_H
