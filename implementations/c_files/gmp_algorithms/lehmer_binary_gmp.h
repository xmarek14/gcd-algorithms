#ifndef GCD_ALGORITHMS_LEHMER_BINARY_GMP_H
#define GCD_ALGORITHMS_LEHMER_BINARY_GMP_H

/**
 * Modified Lehmer's algorithm, inner cycle works with long numbers -> size of shift is limited \n\n
 * !!! MAXIMAL VALUE OF SHIFT IS 63 !!!
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 * @param shift number of the most significant bits to work with (max 63)
 */
void binaryLehmerGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift);

/**
 * Modified Lehmer's algorithm, inner cycle works with long numbers -> size of shift is limited\n
 * Inner cycle stops when n trailing zeros occur\n\n
 * !!! MAXIMAL VALUE OF shift IS 63 !!!
 * !!! MAXIMAL VALUE OF n IS 63 (the implementation uses value n+1 as 0 would break every time) !!!
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 * @param shift number of the most significant bits to work with (max 63)
 * @param n number of trailing zeros to search for
 */
void binaryLehmerWithZerosGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift, int n);

/**
 * Lehmer's algorithm, stops inner cycle when n trailing zeros occur, has
 * no limits for inputs
 * !!! MAXIMAL VALUE OF n IS 63 (the implementation uses value n+1 as 0 would break every time) !!!
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 * @param shift number of the most significant bits to work with
 * @param n number of trailing zeros to search for
 */
void binaryLehmerNoLimitsGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift, int n);

#endif //GCD_ALGORITHMS_LEHMER_BINARY_GMP_H
