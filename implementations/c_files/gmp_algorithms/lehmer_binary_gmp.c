#include "stdbool.h"
#include "gmp.h"
#include "math.h"

#include "lehmer_binary_gmp.h"
#include "../helper_funcs.h"

#define MASK 255

void binaryLehmerGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift) {

    if (shift > 63) {
        mpz_set_ui(g, 0);
        return;
    }
    unsigned long k;

    int lookup[] = {8, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    7, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    mpz_t uZeros, vZeros, tmp, tmp1, tmp2, tmp3, _u, _v;

    mpz_init(uZeros);
    mpz_init(vZeros);
    mpz_init(tmp);
    mpz_init(tmp1);
    mpz_init(tmp2);
    mpz_init(tmp3);
    mpz_init(_u);
    mpz_init(_v);

    mpz_set(_u, u);
    mpz_set(_v, v);


    while (mpz_even_p(_u)) {
        mpz_add_ui(uZeros, uZeros, 1);
        mpz_div_2exp(_u, _u, 1);
    }
    while (mpz_even_p(_v)) {
        mpz_add_ui(vZeros, vZeros, 1);
        mpz_div_2exp(_v, _v, 1);
    }

    if (mpz_cmp(_u, _v) < 0) {
        mpz_swap(_u, _v);
    }

    unsigned long uLen = mpz_sizeinbase(_u, 2);
    unsigned long vLen = mpz_sizeinbase(_v, 2);

    long u1, v1, q;
    mpz_t mpz_u1, mpz_v1;
    mpz_init(mpz_u1);
    mpz_init(mpz_v1);

    while (vLen >= shift) {

        k = uLen - shift;
        mpz_div_2exp(mpz_u1, _u, k);
        u1 = mpz_get_ui(mpz_u1);
        mpz_div_2exp(mpz_v1, _v, k);
        v1 = mpz_get_ui(mpz_v1);

        long a = 1, b = 0, c = 0, d = 1;
        while (true) {

            if (v1 < d || v1 < c) {
                break;
            }

            q = u1 / v1;

            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        if (b == 0) {
            mpz_set(tmp, _v);
            mpz_mod(_v, _u, _v);
            mpz_set(_u, tmp);
        } else {
            mpz_mul_si(tmp1, _u, a);
            mpz_mul_si(tmp2, _v, b);
            mpz_add(tmp3, tmp1, tmp2);
            mpz_abs(tmp3, tmp3);

            mpz_mul_si(tmp1, _u, c);
            mpz_mul_si(tmp2, _v, d);
            mpz_add(tmp1, tmp1, tmp2);
            mpz_abs(tmp1, tmp1);

            mpz_set(_u, tmp3);
            mpz_set(_v, tmp1);
        }

        mpz_div_2exp(_u, _u, lookup[mpz_get_ui(_u) & MASK]);
        mpz_div_2exp(_v, _v, lookup[mpz_get_ui(_v) & MASK]);

        if (mpz_cmp(_u, _v) < 0) {
            mpz_swap(_u, _v);
        }
        uLen = mpz_sizeinbase(_u, 2);
        vLen = mpz_sizeinbase(_v, 2);
    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_set(tmp, _v);
        mpz_mod(_v, _u, _v);
        mpz_set(_u, tmp);
    }

    if (mpz_cmp(uZeros, vZeros) > 0) {
        while (mpz_cmp_ui(vZeros, 0)) {
            mpz_mul_2exp(_u, _u, 1);
            mpz_sub_ui(vZeros, vZeros, 1);
        }
        mpz_set(g, _u);
    } else {
        while (mpz_cmp_ui(uZeros, 0)){
            mpz_mul_2exp(_u, _u, 1);
            mpz_sub_ui(uZeros, uZeros, 1);
        }
        mpz_set(g, _u);
    }
}

void binaryLehmerWithZerosGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift, int n) {

    unsigned long numOfZeros = ((long) 1 << n) - 1;

    if (shift > 63) {
        mpz_set_ui(g, 0);
        return;
    }
    unsigned long k;

    int lookup[] = {8, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    7, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
                    6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    mpz_t uZeros, vZeros, tmp, tmp1, tmp2, tmp3, _u, _v;

    mpz_init(uZeros);
    mpz_init(vZeros);
    mpz_init(tmp);
    mpz_init(tmp1);
    mpz_init(tmp2);
    mpz_init(tmp3);
    mpz_init(_u);
    mpz_init(_v);

    mpz_set(_u, u);
    mpz_set(_v, v);


    while (mpz_even_p(_u)) {
        mpz_add_ui(uZeros, uZeros, 1);
        mpz_div_2exp(_u, _u, 1);
    }
    while (mpz_even_p(_v)) {
        mpz_add_ui(vZeros, vZeros, 1);
        mpz_div_2exp(_v, _v, 1);
    }

    if (mpz_cmp(_u, _v) < 0) {
        mpz_swap(_u, _v);
    }

    unsigned long uLen = mpz_sizeinbase(_u, 2);
    unsigned long vLen = mpz_sizeinbase(_v, 2);

    long u1, v1, u2, v2, q;
    mpz_t mpz_u1, mpz_v1;
    mpz_init(mpz_u1);
    mpz_init(mpz_v1);

    while (vLen >= shift) {

        k = uLen - shift;
        mpz_div_2exp(mpz_u1, _u, k);
        u1 = mpz_get_ui(mpz_u1);
        u2 = mpz_get_ui(_u);
        mpz_div_2exp(mpz_v1, _v, k);
        v1 = mpz_get_ui(mpz_v1);
        v2 = mpz_get_ui(_v);

        long a = 1, b = 0, c = 0, d = 1;
        while (v1 > c && v1 > d) {

            // if there are numOfZeros trailing zeros' algorithm stops
            if (!((a*u2 + b*v2) & numOfZeros) || !((c*u2 + d*v2) & numOfZeros)) {
                break;
            }

            q = u1 / v1;

            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        if (b == 0) {
            mpz_set(tmp, _v);
            mpz_mod(_v, _u, _v);
            mpz_set(_u, tmp);
        } else {
            mpz_mul_si(tmp1, _u, a);
            mpz_mul_si(tmp2, _v, b);
            mpz_add(tmp3, tmp1, tmp2);
            mpz_abs(tmp3, tmp3);

            mpz_mul_si(tmp1, _u, c);
            mpz_mul_si(tmp2, _v, d);
            mpz_add(tmp1, tmp1, tmp2);
            mpz_abs(tmp1, tmp1);

            mpz_set(_u, tmp3);
            mpz_set(_v, tmp1);
        }

        mpz_div_2exp(_u, _u, lookup[mpz_get_ui(_u) & MASK]);
        mpz_div_2exp(_v, _v, lookup[mpz_get_ui(_v) & MASK]);

        if (mpz_cmp(_u, _v) < 0) {
            mpz_swap(_u, _v);
        }
        uLen = mpz_sizeinbase(_u, 2);
        vLen = mpz_sizeinbase(_v, 2);
    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_set(tmp, _v);
        mpz_mod(_v, _u, _v);
        mpz_set(_u, tmp);
    }

    if (mpz_cmp(uZeros, vZeros) > 0) {
        while (mpz_cmp_ui(vZeros, 0)) {
            mpz_mul_2exp(_u, _u, 1);
            mpz_sub_ui(vZeros, vZeros, 1);
        }
        mpz_set(g, _u);
    } else {
        while (mpz_cmp_ui(uZeros, 0)){
            mpz_mul_2exp(_u, _u, 1);
            mpz_sub_ui(uZeros, uZeros, 1);
        }
        mpz_set(g, _u);
    }
}

void binaryLehmerNoLimitsGmp(mpz_t g, mpz_t u, mpz_t v, unsigned long shift, int n) {

    mpz_t mpz_zeros;
    mpz_init(mpz_zeros);
    unsigned long numOfZeros = ((long) 1 << n) - 1;
    mpz_set_ui(mpz_zeros, numOfZeros);

    mpz_t uZeros, vZeros, tmp, tmp1, _u, _v;

    mpz_init(uZeros);
    mpz_init(vZeros);
    mpz_init(tmp);
    mpz_init(tmp1);
    mpz_init(_u);
    mpz_init(_v);

    mpz_set(_u, u);
    mpz_set(_v, v);

    while (mpz_even_p(_u)) {
        mpz_add_ui(uZeros, uZeros, 1);
        mpz_div_2exp(_u, _u, 1);
    }
    while (mpz_even_p(_v)) {
        mpz_add_ui(vZeros, vZeros, 1);
        mpz_div_2exp(_v, _v, 1);
    }

    if (mpz_cmp(_u, _v) < 0) {
        mpz_swap(_u, _v);
    }

    unsigned long uLen = mpz_sizeinbase(_u, 2);
    unsigned long vLen = mpz_sizeinbase(_v, 2);

    unsigned long u2_int, v2_int, k;
    mpz_t u1, v1, u2, v2, q;
    mpz_init(u1);
    mpz_init(v1);
    mpz_init(u2);
    mpz_init(v2);
    mpz_init(q);

    while (vLen >= shift) {

        k = uLen - shift;
        mpz_div_2exp(u1, _u, k);
        mpz_div_2exp(v1, _v, k);

        mpz_mul_2exp(u2, u1, k);
        mpz_sub(u2, _u, u2);
        u2_int = mpz_get_ui(u2);
        mpz_mul_2exp(v2, v1, k);
        mpz_sub(v2, _v, v2);
        v2_int = mpz_get_ui(v2);

        unsigned long a_int, b_int, c_int, d_int;
        mpz_t a, b, c, d;
        mpz_init_set_ui(a, 1);
        mpz_init_set_ui(b, 0);
        mpz_init_set_ui(c, 0);
        mpz_init_set_ui(d, 1);

        while (true) {

            if (mpz_cmp(v1, d) < 0 || mpz_cmp(v1, c) < 0) {
                break;
            }

            a_int = mpz_get_ui(a);
            b_int = mpz_get_ui(b);
            c_int = mpz_get_ui(c);
            d_int = mpz_get_ui(d);
            if (!((a_int*u2_int + b_int*v2_int) & numOfZeros) || !((c_int*u2_int + d_int*v2_int) & numOfZeros)) {
                break;
            }
            mpz_fdiv_q(q, u1, v1);

            mpz_set(tmp, a);
            mpz_set(a, c);
            mpz_mul(tmp1, q, c);
            mpz_sub(c, tmp, tmp1);

            mpz_set(tmp, b);
            mpz_set(b, d);
            mpz_mul(tmp1, q, d);
            mpz_sub(d, tmp, tmp1);

            mpz_set(tmp, u1);
            mpz_set(u1, v1);
            mpz_mul(tmp1, q, v1);
            mpz_sub(v1, tmp, tmp1);
        }

        if (mpz_cmp_ui(b, 0) == 0) {
            mpz_set(tmp, _v);
            mpz_mod(_v, _u, _v);
            mpz_set(_u, tmp);
        } else {
            mpz_mul_2exp(u1, u1, k);
            mpz_mul(tmp, u2, a);
            mpz_mul(tmp1, v2, b);
            mpz_add(tmp, u1, tmp);
            mpz_add(_u, tmp, tmp1);
            mpz_abs(_u, _u);

            mpz_mul_2exp(v1, v1, k);
            mpz_mul(tmp, u2, c);
            mpz_mul(tmp1, v2, d);
            mpz_add(tmp, v1, tmp);
            mpz_add(_v, tmp, tmp1);
            mpz_abs(_v, _v);
        }

        mpz_div_2exp(_u, _u, mpz_scan1(_u, 0));
        mpz_div_2exp(_v, _v, mpz_scan1(_v, 0));

        if (mpz_cmp(_u, _v) < 0) {
            mpz_swap(_u, _v);
        }
        uLen = mpz_sizeinbase(_u, 2);
        vLen = mpz_sizeinbase(_v, 2);

    }
    while (mpz_cmp_ui(_v, 0)) {
        mpz_set(tmp, _v);
        mpz_mod(_v, _u, _v);
        mpz_set(_u, tmp);
    }

    if (mpz_cmp(uZeros, vZeros) > 0) {
        mpz_set(tmp, _u);
        while (mpz_cmp_ui(vZeros, 0)) {
            mpz_mul_2exp(tmp, tmp, 1);
            mpz_sub_ui(vZeros, vZeros, 1);
        }
        mpz_set(g, tmp);
    } else {
        mpz_set(tmp, _u);
        while (mpz_cmp_ui(uZeros, 0)){
            mpz_mul_2exp(tmp, tmp, 1);
            mpz_sub_ui(uZeros, uZeros, 1);
        }
        mpz_set(g, tmp);
    }
}
