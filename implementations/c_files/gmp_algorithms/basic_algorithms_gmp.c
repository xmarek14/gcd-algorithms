#include "gmp.h"

#include "basic_algorithms_gmp.h"
#include "../helper_funcs.h"

#define DECIMAL_PLACES 23
#define VAL(P) mpz_scan1(P, 0)

void euclidGmp(mpz_t g, mpz_t u, mpz_t v) {

    if (mpz_cmp_ui(u, 0) == 0) {
        mpz_set(g, v);
        return;
    }

    mpz_t _u, _v, tmp;
    mpz_init(_u);
    mpz_init(_v);
    mpz_init(tmp);

    mpz_set(_u, u);
    mpz_set(_v, v);
    mpz_set_ui(g, 1);

    if (mpz_cmp(_u, _v) < 0) {
        mpz_set(tmp, _u);
        mpz_set(_u, _v);
        mpz_set(_v, tmp);
    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_mod(tmp, _u, _v);
        mpz_set(_u, _v);
        mpz_set(_v, tmp);
    }
    mpz_set(g, _u);
    mpz_clear(_u);
    mpz_clear(_v);
    mpz_clear(tmp);
}

void leastReminderGmp(mpz_t g, mpz_t u, mpz_t v) {

    mpz_t _u, _v, r, _r;
    mpz_init(_u);
    mpz_init(_v);
    mpz_init(r);
    mpz_init(_r);

    mpz_set(_u, u);
    mpz_set(_v, v);
    mpz_set_ui(g, 1);

    if (mpz_cmp(_u, _v) < 0) {
        mpz_set(r, _u);
        mpz_set(_u, _v);
        mpz_set(_v, r);
    }

    while (mpz_cmp_ui(_v, 0)) {
        mpz_mod(r, _u, _v);
        mpz_sub(_r, v, r);
        mpz_set(_u, _v);
        if (mpz_cmp(r, _r) > 0) {
            mpz_set(_v, _r);
        } else {
            mpz_set(_v, r);
        }
    }
    mpz_set(g, _u);
    mpz_clear(_u);
    mpz_clear(_v);
    mpz_clear(r);
    mpz_clear(_r);
}

void binaryGmp(mpz_t g, mpz_t u, mpz_t v) {

    if (mpz_cmp_ui(u, 0) == 0) {
        mpz_set(g, v);
        return;
    }

    mpz_t _u, _v, tmp;
    mpz_init(_u);
    mpz_init(_v);
    mpz_init(tmp);

    mpz_set(_u, u);
    mpz_set(_v, v);
    mpz_set_ui(g, 1);

    unsigned int uInt = mpz_get_ui(_u);
    unsigned int vInt = mpz_get_ui(_v);
    int uZeros = trailingZeros(uInt);
    int vZeros = trailingZeros(vInt);
    mpz_div_2exp(_u, _u, uZeros);
    mpz_div_2exp(_v, _v, vZeros);

    while (mpz_sgn(_v)) {

        mpz_div_2exp(_u, _u, mpz_even_p(_u));
        mpz_div_2exp(_v, _v, mpz_even_p(_v));
        mpz_div_2exp(_u, _u, mpz_even_p(_u));
        mpz_div_2exp(_v, _v, mpz_even_p(_v));

        if (mpz_odd_p(_u) && mpz_odd_p(_v)) {
            mpz_sub(tmp, _u, _v);
            mpz_abs(tmp, tmp);
            mpz_div_2exp(tmp, tmp, 1);
            mpz_set(_u, _v);
            mpz_set(_v, tmp);
        }

    }

    if (uZeros > vZeros) {
        mpz_mul_2exp(g, _u, vZeros);
    } else {
        mpz_mul_2exp(g, _u, uZeros);
    }
    mpz_clear(_u);
    mpz_clear(_v);
    mpz_clear(tmp);
}
