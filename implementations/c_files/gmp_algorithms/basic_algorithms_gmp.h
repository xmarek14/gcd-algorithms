#ifndef GCD_ALGORITHMS_BASIC_ALGORITHMS_GMP_H
#define GCD_ALGORITHMS_BASIC_ALGORITHMS_GMP_H

/**
 * Euclid's algorithm implemented in GMP library
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 */
void euclidGmp(mpz_t g, mpz_t u, mpz_t v);

/**
 * Euclid's algorithm in GMP, checks value (v - (u % v))
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 * @return gcd(u, v)
 */
void leastReminderGmp(mpz_t g, mpz_t u, mpz_t v);

/**
 * Binary's algorithm implemented in GMP library
 * @param g number to store the gcd to
 * @param u positive number of unlimited size
 * @param v positive number of unlimited size
 */
void binaryGmp(mpz_t g, mpz_t u, mpz_t v);

#endif //GCD_ALGORITHMS_BASIC_ALGORITHMS_GMP_H
