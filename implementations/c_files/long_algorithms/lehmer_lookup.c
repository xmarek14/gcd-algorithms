#include "math.h"
#include "stdlib.h"

#include "../helper_funcs.h"
#include "lehmer_lookup.h"

void precomputation(int* table, int bits) {
    int tmp;
    int nums = (int) pow(2, bits);
    for (int i = 0; i < nums; ++i) {
        for (int j = 0; j < nums; ++j) {
            if (i < j) {
                continue;
            }
            int u = i;
            int v = j;
            int a = 1, b = 0, c = 0, d = 1, q;
            while (v > c && v > d) {
                q = u / v;

                tmp = a;
                a = c;
                c = tmp - q*c;

                tmp = b;
                b = d;
                d = tmp - q*d;

                tmp = u;
                u = v;
                v = tmp - q*v;
            }
            table[4 * (nums*i + j)    ] = a;
            table[4 * (nums*i + j) + 1] = b;
            table[4 * (nums*i + j) + 2] = c;
            table[4 * (nums*i + j) + 3] = d;
        }
    }
}

long lookupLehmer(long u, long v, int shift, const int* table) {

    int nums = (int) pow(2, shift);
    int a, b, c, d;
    unsigned int k;
    long tmp;
    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }
    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    while (vLen >= shift) {

        k = uLen - shift;
        int u1 = (int) (u >> k);
        int v1 = (int) (v >> k);

        a = table[4*(nums * v1 + u1)    ];
        b = table[4*(nums * v1 + u1) + 1];
        c = table[4*(nums * v1 + u1) + 2];
        d = table[4*(nums * v1 + u1) + 3];

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            tmp = v;
            v = labs(c*u + d*v);
            u = labs(a*u + b*tmp);
        }
        if (u < v) {
            tmp = u;
            u = v;
            v = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }
    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }
    return u;
}