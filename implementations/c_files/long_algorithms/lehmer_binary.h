#ifndef GCD_ALGORITHMS_LEHMER_BINARY_H
#define GCD_ALGORITHMS_LEHMER_BINARY_H

/**
 * Lehmer's algorithm, stops if n trailing zeros are found (in case the parameter
 * is included). These are then removed using LUT of size 64.
 * @param u positive input number
 * @param v positive input number
 * @param shift number of bits to take for inner cycle
 * @param n number of trailing zeros to search for
 * @return gcd(u, v)
 */
long binaryLehmerWithZeros(long u, long v, int shift, int n);

/**
 * Lehmer's algorithm combined with binary.
 * @param u positive input number
 * @param v positive input number
 * @param shift number of bits to take for inner cycle
 * @return gcd(u, v)
 */
long binaryLehmer(long u, long v, int shift);

/**
 * Lehmer's algorithm, instead of inner cycle uses LUT. Contains static table
 * of up to 8 trailing zeros.
 * @param u positive input number
 * @param v positive input number
 * @param shift number of bits to take for table look up
 * @param table LUT containing values of a, b, c, d for specific u, v
 * @return gcd(u, v)
 */
long binaryLookupLehmer(long u, long v, int shift, const int* table);

#endif //GCD_ALGORITHMS_LEHMER_BINARY_H
