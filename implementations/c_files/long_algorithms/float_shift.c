#include "stdbool.h"

#include "../helper_funcs.h"

#define DECIMAL_PLACES 23
#define BINARY_BASE 2

int getShift(float f) {
    unsigned int fInt = (unsigned int) ((f - (int)f) * (1 << DECIMAL_PLACES));

    if (fInt == 0) {
        return 0;
    }

    int sequence = 1;
    while ((fInt >> 1) & fInt){
        sequence++;
        fInt &= fInt >> 1;
    }

    // searches for most significant one left in fInt
    while (true) {
        int zeros = trailingZeros(fInt);
        if (!(fInt & (0xFFFFFFFF << (zeros + 1)))) {
            return (DECIMAL_PLACES - (zeros + sequence));
        }
        fInt &= 0xFFFFFFFF << (zeros + 1);
    }
}

long shifting(long u, long v) {

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    float f;
    unsigned int shift;
    long tmp;
    while (v != 0) {
        f = (float) u / (float) v;
        shift = getShift(f);
        f *= 1 << shift;

        tmp = u * (long)(1 << shift) - (long)(f + (int)(2*f - 2*(int)(f))) * v;
        tmp = ((tmp < 0) * (-2) + 1) * tmp;
        u = v;
        v = tmp;
    }

    u >>= trailingZeros(u);
    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}

long division(long u,  long v)
{
    double f = (double) u / (double) v;
    int _f = (int) f;
    long tmp;

    while (!equalIntDouble(_f, f)) {

        tmp = (long) (u - _f * v);
        u = v;
        v = tmp;

        f = (double) u / (double) v;
        _f = (int) f;
    }
    return v;
}
