#include "stdbool.h"
#include "stdlib.h"
#include "math.h"

#include "../helper_funcs.h"
#include "lehmer_binary.h"

#define MASK 63

long binaryLehmerWithZeros(long u, long v, int shift, int n) {

    unsigned long numOfZeros = ((long) 1 << n) - 1;
    long tmp, k;
    int lookup[] = {6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    long u1, v1, u2, v2, q;
    while (vLen >= shift) {

        k = uLen - shift;
        u1 = u >> k;
        v1 = v >> k;
        u2 = u & (((long) 1 << k) - 1);
        v2 = v & (((long) 1 << k) - 1);

        long a = 1, b = 0, c = 0, d = 1;
        while (v1 > c && v1 > d) {

            // if there are numOfZeros trailing zeros' algorithm stops
            if (!((a*u2 + b*v2) & numOfZeros) || !((c*u2 + d*v2) & numOfZeros)) {
                break;
            }

            q = u1 / v1;

            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            u = labs((u1 << k) + a * u2 + b * v2);
            v = labs((v1 << k) + c * u2 + d * v2);
        }

        u >>= lookup[u & MASK];
        v >>= lookup[v & MASK];

        if (u < v) {
            tmp = v;
            v = u;
            u = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }

    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}

long binaryLehmer(long u, long v, int shift) {

    long tmp, k;
    int lookup[] = {6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    long u1, v1, u2, v2, q;
    while (vLen >= shift) {

        k = uLen - shift;
        u1 = u >> k;
        v1 = v >> k;
        u2 = u & (((long) 1 << k) - 1);
        v2 = v & (((long) 1 << k) - 1);

        long a = 1, b = 0, c = 0, d = 1;
        while (v1 > c && v1 > d) {

            q = u1 / v1;

            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            u = labs((u1 << k) + a * u2 + b * v2);
            v = labs((v1 << k) + c * u2 + d * v2);
        }

        u >>= lookup[u & MASK];
        v >>= lookup[v & MASK];

        if (u < v) {
            tmp = v;
            v = u;
            u = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }

    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}

long binaryLookupLehmer(long u, long v, int shift, const int* table) {

    int nums = (int) pow(2, shift);
    int k, a, b, c, d;
    long tmp;
    int lookup[] = {6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }
    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    while (vLen >= shift) {

        k = (int) uLen - shift;
        int u1 = (int) (u >> k);
        int v1 = (int) (v >> k);

        a = table[4*(nums * v1 + u1)    ];
        b = table[4*(nums * v1 + u1) + 1];
        c = table[4*(nums * v1 + u1) + 2];
        d = table[4*(nums * v1 + u1) + 3];

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            tmp = v;
            v = labs(c*u + d*v);
            u = labs(a*u + b*tmp);
        }
        u >>= lookup[u & MASK];
        v >>= lookup[v & MASK];

        if (u < v) {
            tmp = u;
            u = v;
            v = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }
    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }
    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}

