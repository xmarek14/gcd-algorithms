#ifndef GCD_ALGORITHMS_EUCLID_LOWER_BITS_H
#define GCD_ALGORITHMS_EUCLID_LOWER_BITS_H

/**
 * Equation (u + qv) % (2^k) = 0 has infinite number of solutions\n
 * Add param n such, that (u + (q + n(2^k)) v) % (2^k) = 0\n
 * This function finds such n, so value (u + (q + n(2^k)) v) is smallest possible\n
 * @param u input param meeting equation above
 * @param v input param meeting equation above
 * @param q input param meeting equation above
 * @param k input param meeting equation above
 * @return optimal value of n
 */
int approximateToZero(long u, long v, int q, int k);

/**
 * Helper function for precomputation phase
 * @param i input integer
 * @param j input integer
 * @param modulo input integer
 * @return value q such, that (i + qj) % modulo = 0
 */
int findQ(int i, int j, int modulo);

/**
 * Creates table of q for every tuple (i, j); i < 2^k; j < 2^k such, that\n
 * i + qj has k trailing zeros
 * @param k number of trailing zeros
 * @param table table to be filled
 */
void combinedPrecomputation(int k, int* table);

/**
 * Euclid's algorithm implemented in a way to operate on the least significant bits.
 * @param u positive input integer
 * @param v positive input integer
 * @param k number of trailing zeros to work with
 * @param table precomputed table of q values
 * @return gcd(u, v)
 */
long combinedEuclid(long u, long v, int k, const int* table);

#endif //GCD_ALGORITHMS_EUCLID_LOWER_BITS_H
