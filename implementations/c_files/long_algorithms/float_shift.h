#ifndef GCD_ALGORITHMS_FLOAT_SHIFT_H
#define GCD_ALGORITHMS_FLOAT_SHIFT_H

/**
 * Helper function for shifting, searches for optimal zero sequence
 * @param f decimal number to search for longest zero series in
 * @return optimal shift to get the most zeros behind decimal point
 */
int getShift(float f);

/**
 * Computes ratio u/v and uses shifted value to create u', v'
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long shifting(long u, long v);

/**
 * Different implementation of Euclid's algorithm \n
 * !!! PROBLEMATIC BECAUSE OF COMPARING INT AND DOUBLE (ACCURACY) !!!
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long division(long u,  long v);

#endif //GCD_ALGORITHMS_FLOAT_SHIFT_H
