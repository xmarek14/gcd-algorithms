#ifndef GCD_ALGORITHMS_LEHMER_LOWER_BITS_H
#define GCD_ALGORITHMS_LEHMER_LOWER_BITS_H

/**
 * Lehmer's algorithm with inner cycle operating on lower bits.\n
 * !!! ATTENTION: PROBLEMATIC ON LARGE NUMBERS, AS a*u + b*v CAN BE LARGER
 * THAN 64bit LONG! (max recommended size of inputs is 60 bits)
 * @param u positive input number
 * @param v positive input number
 * @param shift number of bits to take for inner cycle
 * @return gcd(u, v)
 */
long lowerBitsLehmer(long u, long v, int shift);

#endif //GCD_ALGORITHMS_LEHMER_LOWER_BITS_H
