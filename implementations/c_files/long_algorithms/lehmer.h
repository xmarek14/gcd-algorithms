#ifndef GCD_ALGORITHMS_LEHMER_H
#define GCD_ALGORITHMS_LEHMER_H

/**
 * Implementation of Lehmer's algorithm
 * @param u positive input integer
 * @param v positive input integer
 * @param shift number of bits to work with in inner cycle
 * @return gcd(u, v)
 */
long lehmerOrig(long u, long v, int shift);

/**
 * Implementation of Lehmer's algorithm with slight modifications
 * @param u positive input integer
 * @param v positive input integer
 * @param shift number of bits to work with in inner cycle
 * @return gcd(u, v)
 */
long myLehmer(long u, long v, int shift);

/**
 * Lehmer's algorithm, takes shift bits of both u and v
 * @param u positive input integer
 * @param v positive input integer
 * @param shift number of bits to work with in inner cycle
 * @return gcd(u, v)
 */
long sameShiftLehmer(long u, long v, int shift);

#endif //GCD_ALGORITHMS_LEHMER_H
