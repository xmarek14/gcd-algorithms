#ifndef GCD_ALGORITHMS_BASIC_ALGORITHMS_H
#define GCD_ALGORITHMS_BASIC_ALGORITHMS_H

/**
 * Implementation of Euclid's algorithm
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long euclid(long u, long v);

/**
 * Euclid's algorithm, checks value (v - (u % v))
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long leastReminder(long u, long v);

/**
 * Implementation of binary algorithm
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long binary(long u, long v);

/**
 * Binary algorithm using lookup table to remove trailing zeros
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u, v)
 */
long binaryLookup(long u, long v);

#endif //GCD_ALGORITHMS_BASIC_ALGORITHMS_H
