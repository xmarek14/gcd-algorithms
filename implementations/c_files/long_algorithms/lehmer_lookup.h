#ifndef GCD_ALGORITHMS_LEHMER_LOOKUP_H
#define GCD_ALGORITHMS_LEHMER_LOOKUP_H

/**
 * Computes LUT for Lehmer's algorithm.
 * @param table table of size (4 * (maxNum * (maxNum - 1) + maxNum - 1) + 3),
 * where maxNum = 2^bits
 * @param bits number of bits of the biggest number in LUT
 */
void precomputation(int* table, int bits);

/**
 * Lehmer's algorithm, instead inner cycle uses LUT. \n\n
 * !!! shift PARAM HAS TO MATCH bits PARAM IN PRECOMPUTATION PHASE !!! \n\n
 * @param u positive input number
 * @param v positive input number
 * @param shift number of bits to take for table look up
 * @param table LUT containing values of a, b, c, d for specific u, v
 * @return gcd(u, v)
 */
long lookupLehmer(long u, long v, int shift, const int* table);

#endif //GCD_ALGORITHMS_LEHMER_LOOKUP_H
