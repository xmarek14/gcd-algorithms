#include "../helper_funcs.h"
#include "basic_algorithms.h"

#define MASK 63

long euclid(long u, long v) {
    long tmp;
    while (v) {
        tmp = u % v;
        u = v;
        v = tmp;
    }
    return u;
}

long leastReminder(long u, long v) {
    long tmp;
    while (v) {
        tmp = u % v;
        u = v;
        v = tmp - ((v - 2*tmp) >> 63) * (v - 2*tmp);
    }
    return u;
}

long binary(long u, long v) {

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    long tmp;
    while (v) {

        u >>= (!(u & 1));
        v >>= (!(v & 1));
        u >>= (!(u & 1));
        v >>= (!(v & 1));

        if (u & 1 && v & 1) {
            tmp = u - v;
            tmp *= 2 * (tmp > 0) - 1;       //absolute value
            tmp >>= 1;
            u = v;
            v = tmp;
        }

    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}

long binaryLookup(long u, long v) {

    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    int lookup[] = {6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0,
                    1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1,
                    0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
                    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2,
                    0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    while (v) {

        u >>= lookup[u & MASK];
        v >>= lookup[v & MASK];

        //puts smaller into v, bigger - smaller into u
        u = (u + v) - ((v = (v ^ ((u ^ v) & -(u < v)))) << 1);

    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}