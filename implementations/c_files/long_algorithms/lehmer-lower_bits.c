#include "stdlib.h"

#include "lehmer-lower_bits.h"
#include "../helper_funcs.h"

long lowerBitsLehmer(long u, long v, int shift) {

    long tmp;
    int mask = (1 << shift) - 1;
    int uZeros = trailingZeros(u);
    int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int vLen = bitLenDeBruijn(v);

    long u3, v3;
    while (vLen >= shift) {

        u3 = u & mask;
        v3 = v & mask;

        long a = 1, b = 0, c = 0, d = 1;
        /**
         * In purpose of simplification of the masking, index does not hold
         * the index of bit, but value 2^real_index.
         */
        int index = 1;
        long _v3 = v3 + 1;
        while (v3 > 0 && v3 != _v3) {

            _v3 = v3;
            if (u3 & index & v3) {
                tmp = a;
                a = c;
                c = tmp - c;

                tmp = b;
                b = d;
                d = tmp - d;

                tmp = u3;
                u3 = v3;
                v3 = tmp - v3;
            }

            else if (u3 & index) {
                u3 <<= 1;
                a <<= 1;
                b <<= 1;
                index <<= 1;
            }

            else if (v3 & index) {
                v3 <<= 1;
                c <<= 1;
                d <<= 1;
                index <<= 1;
            }

            else {
                index <<= 1;
                a <<= 1;
                b <<= 1;
                c <<= 1;
                d <<= 1;
            }
        }

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            /** PROBLEMATIC PART - POSSIBILITY OF OVERFLOW */
            tmp = labs(a * u + b * v);
            v = labs(c * u + d * v);
            u = tmp;
        }

        u >>= trailingZeros(u);
        v >>= trailingZeros(v);

        if (u < v) {
            tmp = v;
            v = u;
            u = tmp;
        }
        vLen = bitLenDeBruijn(v);
    }

    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}
