#include "stdlib.h"
#include "math.h"

#include "../helper_funcs.h"
#include "euclid_lower_bits.h"

int approximateToZero(long u, long v, int q, int k) {
    long numerator = - u - q*v;
    long denominator = (1<<k) * v;
    double n = (double) numerator / (double) denominator;
    if (n - (int)n >= 0.5) {
        return ceil(n);
    }
    return floor(n);
}

int findQ(int i, int j, int modulo) {
    int mul = 0;
    if ((i + mul*j) % modulo == 0) { return mul; }
    while (1) {
        mul++;
        if ((i + mul*j) % modulo == 0) { return mul; }
        mul *= -1;
        if ((i + mul*j) % modulo == 0) { return mul; }
        mul *= -1;
        if (mul > modulo) { return 0; }
    }
}

void combinedPrecomputation(int k, int* table) {
    for (int i = 1; i < (1<<k); i += 2) {
        for (int j = 1; j < (1<<k); j += 2) {
            int s = findQ(i, j, 1<<k);
            int index = (i-1)/2 * (1<<(k-1)) + (j-1)/2;
            table[index] = s;
        }
    }
}

long combinedEuclid(long u, long v, int k, const int* table) {

    unsigned int uZeros = trailingZeros(u);
    unsigned int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    long tmp;
    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    while (v != 0) {
        tmp = u;
        u = v;

        int i = (int) tmp & ((1 << k) - 1);
        int j = (int) v & ((1 << k) - 1);
        int q = table[(i-1)/2 * (1 << (k-1)) + (j-1)/2];

        int n = approximateToZero(tmp, v, q, k);
        v = labs(tmp + (q + n*(1<<k))*v);

        v >>= trailingZeros(v);
        if (u < v) {
            tmp = u;
            u = v;
            v = tmp;
        }
    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}
