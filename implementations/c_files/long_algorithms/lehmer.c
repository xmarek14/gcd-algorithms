#include "stdlib.h"
#include "math.h"

#include "../helper_funcs.h"
#include "lehmer.h"

long lehmerOrig(long u, long v, int shift) {

    int base = 2;
    long tmp, k;
    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int uLen, vLen;
    long u1, v1;
    while (v >= base) {
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
        k = uLen - shift;
        if (uLen >= shift) {
            u1 = u >> k;
            if (vLen >= k) {
                v1 = v >> k;
            } else {
                v1 = 0;
            }
        } else {
            u1 = u;
            v1 = v;
        }

        long a = 1, b = 0, c = 0, d = 1;
        while ((v1 + c) != 0 && (v1 + d) != 0) {
            long q = (long) ((double)(u1 + a) / (double)(v1 + c));
            long _q = (long) ((double)(u1 + b) / (double)(v1 + d));

            if (q != _q) {
                break;
            }

            tmp = c;
            c = a - q * c;
            a = tmp;

            tmp = d;
            d = b - q * d;
            b = tmp;

            tmp = v1;
            v1 = u1 - q * v1;
            u1 = tmp;
        }

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            tmp = c * u + d * v;
            u = a * u + b * v;
            v = tmp;
        }
    }

    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }
    return u;
}


long myLehmer(long u, long v, int shift) {
    long tmp, k;

    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    long u1, v1, u2, v2, q;
    while (vLen >= shift) {
        k = uLen - shift;
        u1 = u >> k;
        v1 = v >> k;
        u2 = u & (((long) 1 << k) - 1);
        v2 = v & (((long) 1 << k) - 1);

        long a = 1, b = 0, c = 0, d = 1;
        while (v1 > d && v1 > c) {
            q = u1 / v1;
            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else {
            u = labs((u1 << k) + a * u2 + b * v2);
            v = labs((v1 << k) + c * u2 + d * v2);
        }
        if (u < v) {
            tmp = v;
            v = u;
            u = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }
    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }

    return u;
}


long sameShiftLehmer(long u, long v, int shift) {

    unsigned int uZeros = trailingZeros(u);
    unsigned int vZeros = trailingZeros(v);
    u >>= uZeros;
    v >>= vZeros;

    long tmp;
    if (u < v) {
        tmp = u;
        u = v;
        v = tmp;
    }

    unsigned int uLen = bitLenDeBruijn(u);
    unsigned int vLen = bitLenDeBruijn(v);

    long u1, v1, q;
    while (vLen >= shift) {

        u1 = u >> (uLen - shift);
        v1 = v >> (vLen - shift);

        long a = 1, b = 0, c = 0, d = 1;
        bool swapped = false;
        if (u1 < v1) {
            tmp = u1;
            u1 = v1;
            v1 = tmp;
            swapped = true;
        }

        while (v1 > d && v1 > c) {
            q = u1 / v1;
            c  = (c + a)   - (q + 1)*(a = c)  ;
            d  = (d + b)   - (q + 1)*(b = d)  ;
            v1 = (u1 + v1) - (q + 1)*(u1 = v1);
        }

        int shiftOfV = pow(2, uLen - vLen);
        if (b == 0) {
            tmp = v;
            v = u % v;
            u = tmp;
        } else if (swapped) {
            u = labs(b*u + a*v*shiftOfV);
            v = labs(d*u + c*v*shiftOfV);
        } else {
            u = labs(a*u + b*v*shiftOfV);
            v = labs(c*u + d*v*shiftOfV);
        }

        u >>= trailingZeros(u);
        v >>= trailingZeros(v);

        if (u < v) {
            tmp = v;
            v = u;
            u = tmp;
        }
        uLen = bitLenDeBruijn(u);
        vLen = bitLenDeBruijn(v);
    }
    while (v != 0) {
        tmp = v;
        v = u % v;
        u = tmp;
    }

    return u << (vZeros ^ ((uZeros ^ vZeros) & -(uZeros < vZeros)));
}
