#include "stdio.h"
#include "stdint.h"
#include "ctype.h"
#include "math.h"

#include "helper_funcs.h"

void printBin(unsigned long x) {
    for (int i = 64; i > 0; --i) {
        if (x & (1 << i)) {
            printf("1");
        } else {
            printf("0");
        }
    }
}

unsigned int bitLenDeBruijn(unsigned long x) {
    static const int MultiplyDeBruijnBitPosition[32] =
            {
                    0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22,
                    25, 3, 30,8, 12, 20, 28, 15, 17, 24, 7, 19,
                    27, 23, 6, 26, 5, 4, 31
            };
    if (!(x >> 32)) {

        unsigned int v = x & 0xffffffff;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;

        return MultiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27] + 1;
    }
    return 32 + bitLenDeBruijn(x >> 32);
}

int trailingZeros(unsigned long x) {
    static const int MultiplyDeBruijnBitPosition[32] =
            {
                    0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25,
                    17, 4, 8,31, 27, 13, 23, 21, 19, 16, 7, 26,
                    12, 18, 6, 11, 5, 10, 9
            };
    return MultiplyDeBruijnBitPosition[((uint32_t)((x & -x) * 0x077CB531U)) >> 27];
}

long gcd(long u, long v) {
    long tmp;
    while (v) {
        tmp = u % v;
        u = v;
        v = tmp;
    }
    return u;
}

bool equalIntDouble(int x, double y) {
    return fabs(x - y) < 0.0000000001;
}

bool isNumber(const char number[])
{
    int i = 0;

    for (; number[i] != 0; i++)
    {
        if (!isdigit(number[i]))
            return false;
    }
    return true;
}