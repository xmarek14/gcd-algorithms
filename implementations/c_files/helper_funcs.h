#include "stdbool.h"

#ifndef GCD_ALGORITHMS_HELPER_FUNCS_H
#define GCD_ALGORITHMS_HELPER_FUNCS_H

/**
 * Print long integer in binary.
 * @param x number to print
 */
void printBin(unsigned long x);

/**
 * Returns length of x in binary, maximal length is 64.
 * @param x number to get length of
 * @return binary length of x
 */
unsigned int bitLenDeBruijn(unsigned long x);

/**
 * Counts and returns trailing zeros ing x
 * @param x number to get trailing zeros of
 * @return trailing zeros of x
 */
int trailingZeros(unsigned long x);

/**
 * Euclid's algorithm.
 * @param u positive input integer
 * @param v positive input integer
 * @return gcd(u ,v)
 */
long gcd(long u, long v);

/**
 * Compares int and double numbers.
 * @param x Int number to compare
 * @param y Double number to compare
 * @return true if the difference is smaller than 10^(-10), else false
 */
bool equalIntDouble(int x, double y);

/**
 * Check if string is a number
 * @param number string to check
 * @return true if the string contains only digits, false else
 */
bool isNumber(const char number[]);

#endif //GCD_ALGORITHMS_HELPER_FUNCS_H
