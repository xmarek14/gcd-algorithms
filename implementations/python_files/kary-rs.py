import math

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        if x % m > m / 2:
            return x % m - m
        else:
            return x % m

def gcd(a, b):

    while a != b:
        if a == 0: return b
        if b == 0: return a
        if a > b:
            a = a % b
        else:
            b = b % a

    return a

def kary(u, v, k):

    # precomputation of the tables

    G = [] #gcd(x, k)
    for i in range(k):
        G.append(gcd(i, k))

    #G[x] == 1 => 1/x mod k
    I = []
    for i in range(len(G)):
        if G[i] == 1:
            I.append(modinv(i, k))
        else:
            I.append(None)

    A = [None] * k
    A_sum_of_a_b = [k] * k
    k_sqrt = math.ceil(math.sqrt(k) + 1)
    for a in range(0, k_sqrt):
        for b in range(-k_sqrt + 1, k_sqrt):

            for modulo in range (-k_sqrt + 1, k_sqrt):
                if a != 0 and b != 0 and (b + (modulo * k)) % (-a) == 0:
                    x = (b + (modulo * k)) // (-a)
                    if x >= 0 and x < k and A_sum_of_a_b[x] > abs(a) + abs(b):
                        A[x] = a
                        A_sum_of_a_b[x] = abs(a) + abs(b)


    _P = [True for _ in range(k+1)]
    index = 2
    while (index * index <= k):
 
        if _P[index]:
            for i in range(index * index, k+1, index):
                _P[i] = False
        index += 1
 
    P = [] #primes smaller or equal to k
    for p in range(2, k+1):
        if _P[p] and (p < math.sqrt(k) + 1 or k % p == 0):
            P.append(p)

    print(G, I, A, P, sep="\n")

    g = 1
    for d in P:
        while u % d == 0 and v % d == 0:
            u //= d 
            v //= d 
            g *= d 
    
    # main loop

    while u != 0 and v != 0:

        _u = u % k 
        _v = v % k 
        if G[_u] > 1: u //= G[_u]
        elif G[_v] > 1: v //= G[_v]
        else:
            x = (_u * I[_v]) % k
            a = A[x]
            b = (-a * x) % k
            if b > k/2: b -= k
            if u > v: u = abs(a*u + b*v) // k 
            else: v = abs(a*u + b*v) // k

    # final trial division

    if v == 0: t = u
    else: t = v 
    for d in P:
        while t % d == 0:
            t //= d

    return g*t


