from random import randrange

def simple_modulo_gcd(u, v):

    step_count = 0

    while v != 0:
        step_count += 1
        u, v = v, u % v

    return step_count

def negative_modulo_gcd(u, v):

    step_count = 0

    while v != 0:
        step_count += 1
        r = u % v
        _r = v - r
        if _r < r:
            u, v = v, _r
        else:
            u, v = v, r

    return step_count


def steps_compare_gcd(tests, size):
    total_steps = [0, 0]
    avg_steps = [0, 0]
    
    for _ in range (tests):
        u = randrange(2**size)
        v = randrange(2**size)
        
        total_steps[0] += simple_modulo_gcd(u, v)
        total_steps[1] += negative_modulo_gcd(u, v)

    for i in range (len(avg_steps)):
        avg_steps[i] = round(total_steps[i] / tests, 2)

    print("Euclid's algorithm: ", avg_steps[0])
    print("Negative modulo:    ", avg_steps[1])

def run_steps_tests():
    for i in [16, 32, 64, 128, 256, 512]:
        for j in range(1):
            print("TEST NR.", j+1, "WITH", i, "BIT NUMBERS:")
            steps_compare_gcd(10000, i)
            
            
run_steps_tests()