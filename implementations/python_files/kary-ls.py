import math

def gcd(a, b):

    while a != b:
        if a == 0: return b
        if b == 0: return a
        if a > b:
            a = a % b
        else:
            b = b % a

    return a
    
def precomputation(k):

    # A and B stores values to minimalize u = at - bu (fraction b/a)
    # is close to i/k^2, these tables are precomputed using only k,
    # so it can be reused with different u and v
    A = [0] * (k*k + 1)
    B = [0] * (k*k + 1)
    A[0] = 1

    A[-1] = 1
    B[-1] = 1
    

    for a in range(1, k+1):
        for b in range(1, a):
            if gcd(a, b) == 1:
                i = math.floor(k**2 * b/a + 0.5)
                print(a, b, i)
                A[i] = a
                B[i] = b

    
    left = 0
    right = 0
    for i in range(0, k*k+1):

        if right < i:
            right = i
            while right < len(A) - 1 and A[right] == 0:
                right += 1
        if A[i] != 0:
            left = i
        else:
            mediant = (B[left] + B[right]) / (A[left] + A[right])
            if i < k*k*mediant:
                A[i] = A[left]
                B[i] = B[left]
            else:
                A[i] = A[right]
                B[i] = B[right]

    _P = [True for i in range(k+1)]
    index = 2
    while (index * index <= k):

        if _P[index]:
            for i in range(index * index, k+1, index):
                _P[i] = False
        index += 1

    # table P stores all primes smaller than k, is used during trial
    # division to remove all divisors smaller than k (just like 2 in
    # binary algorithm)
    P = []
    for p in range(2, k+1):
        if _P[p]:
            P.append(p)

    print(A, B, P)
    return (A, B, P)

precomputation(4)

def lskary(u, v, k):

    A, B, P = precomputation(k)

    g = 1
    for d in P:                                                     # removing multiples of primes smaller than k,
        while u % d == 0 and v % d == 0:                            # that are common for u and v, storing them into g
            u //= d
            v //= d
            g *= d

    if u < v:
        u, v = v, u

    # main loop
    while v != 0:
        print(u, v, end="\t")
        e = math.log(u/v, k)
        t = k**(math.floor(e))*v                                    # t = k^e * v, such that t <= u <= kt
        if t == 0:
            t = v

        while t <= u:                                               # gets the last multiple of t below u
            t *= k
        while t > u:
            t //= k

        x = round(t/u, math.floor(2*math.log(k, 2)))
        i = math.floor(k**2 * x + 1/2)                              # +1/2 is just to simulate real rounding
        a = A[i]
        b = B[i]
        print(a, b)
        u = abs(a*t - b*u)                                          # recount of u, where it's reduced
        if u < v:
            u, v = v, u

    for d in P:                                                      # removing multiples of primes smaller than k
        while u % d == 0:
            u //= d

    return(int(g*u))


